# pydevdを使ったRemode_debug


1.pipでpydevd-pycharm をinstall

- JetBrainのToolBoxでPyCharmを管理すると、pydevd-pycharmがバージョンフォルダ(191.8026.44)にinstallされる。
- JetBrainののToolBoxは使わずに、手動でPyCharmをinstallした方がpathが分かりやすい

![icon](image_files/pydevd_00.jpg)


2.PyCharm上で、設定を行う

![icon](image_files/pydevd_01.jpg)

- localhost nameは"localhost"が良い。localhost以外にするとNukeでエラーになった。


3.PyCharmからDebugを実行

![icon](image_files/pydevd_02.jpg)

- debug serverが待機に入る

![icon](image_files/pydevd_03.jpg)


4.Maya上で、pydevdを実行
- import pydevd-pycharmと打たなくても動く。


```python

EGG_FILENAME = r"C:\Program Files\JetBrains\PyCharm 2019.1.2\debug-eggs\pydevd-pycharm.egg"
HOST = 'localhost'
PORT = 9999

# PyCharm付属のeggファイルにパスを通す
import sys
if EGG_FILENAME not in sys.path:
    sys.path.append(EGG_FILENAME)

# 起動
import pydevd
pydevd.settrace(HOST, port=PORT, stdoutToServer=True, stderrToServer=True)

```

- debug serverに接続される

![icon](image_files/pydevd_04.jpg)

5.Mayaが操作をうけつけなくなるので、PyCharm側でResume(F9)を行う

![icon](image_files/pydevd_05.jpg)

6.Maya上で、debugしたい.pyを下記のように実行

```python

# パスは環境に応じて変更してください
FILENAME = r"C:\Users\h-sato\source\repos\cgworld_cc2019"

# PyCharm付属のeggファイルにパスを通す
import sys
if FILENAME not in sys.path:
    sys.path.append(FILENAME)

# 起動
import pyside2_sample_basic
reload(pyside2_sample_basic)
form = pyside2_sample_basic.Form()
form.show()

```

7.break pointが設定されていれば、step overすると、break pointで止まる。


8.停止するときは、以下のように、まずはmaya上で以下を実行。

```python

import pydevd
pydevd.stoptrace()

```

- pydev serverが Waitingに入る

![icon](image_files/pydevd_06.jpg)


9.更にPyCharm側でStopすると、pydev serverが 停止。

![icon](image_files//pydevd_07.jpg)
