# -*- coding: utf-8 -*-

import sys
import os
from PySide2 import QtWidgets
from PySide2 import QtCore
from PySide2 import QtGui

# -------maya-------
from functools import partial
import pymel
import pymel.core as pm
import maya.standalone


class Form(QtWidgets.QDialog):
    def __init__(self, parent=None):
        super(Form, self).__init__(parent)
        self.setWindowTitle("Sample GUI")
        self.resize(200, 75)
        
        # Layout
        layout = QtWidgets.QVBoxLayout()

        # QSpinBox
        self.spinBox = QtWidgets.QSpinBox()
        self.spinBox.setMinimum(1)
        self.spinBox.setMaximum(10)
        layout.addWidget(self.spinBox)

        # 実行ボタン
        self.button01 = QtWidgets.QPushButton("make Sphere")
        layout.addWidget(self.button01)

        # layout
        self.setLayout(layout)

        # signal
        self.button01.clicked.connect(self.make_sphere)

    def make_sphere(self):
        count = int(self.spinBox.value())  # spinBoxの値を取得

        for i in range(count):  # 取得した値を0から順番にfor文で回します
            pm.sphere()  # sphereを作成
            x_pos = i * 5  # x座標
            y_pos = i * 1  # y座標

            pm.move([x_pos, y_pos, 0])  # 更にsphereの位置を変更


if __name__ == '__main__':
    form = Form()    
    form.show()
