# -*- coding: utf-8 -*-

import sys
import os
import logging
from PySide2 import QtWidgets
from PySide2 import QtCore
from PySide2 import QtGui


# -------maya module-------
from functools import partial
import pymel
import pymel.core as pm
import maya.standalone

sys.dont_write_bytecode = True  # pycを作らないようにします

# -----SystemLog-----
logger = logging.getLogger('SystemLog')  # ログの出力名を設定
logger.setLevel(10)
# ログレベルの設定
# DEBUG     10  おもに問題を診断するときにのみ関心があるような、詳細な情報
# INFO      20  想定された通りのことが起こったことの確認
# WARNING   30  想定外のことが発生
# ERROR     40  より重大な問題により、ソフトウェアがある機能を実行できない
# CRITICAL  50  プログラム自体が実行を続けられないことを表す

# ログのファイル出力先を設定
myname = os.environ.get("USERNAME")
CURRENT_PATH = os.path.dirname(__file__)
log_filename = "system_" + myname + ".log"
log_path = os.path.join(CURRENT_PATH, "_log", "SystemLog", log_filename)

# ログの保存先を作成します
if not os.path.isdir(os.path.dirname(log_path)):
    os.makedirs(os.path.dirname(log_path))

fh = logging.FileHandler(log_path, mode='w')  # aは追記, wは上書
logger.addHandler(fh)

# ログのコンソール出力の設定
sh = logging.StreamHandler()
logger.addHandler(sh)

# ログの出力形式の設定
formatter = logging.Formatter('%(asctime)s:%(name)s:%(lineno)d:%(levelname)s:%(message)s')
fh.setFormatter(formatter)
sh.setFormatter(formatter)
logger.debug('----logger start----')


class Form(QtWidgets.QDialog):
    def __init__(self, *args):
        super(Form, self).__init__(*args)
        self.setWindowTitle("Sample GUI")
        self.resize(200, 75)

        # Layout
        layout = QtWidgets.QVBoxLayout()

        # QSpinBox
        self.spinBox = QtWidgets.QSpinBox()
        self.spinBox.setMinimum(1)
        self.spinBox.setMaximum(10)
        layout.addWidget(self.spinBox)

        # 実行ボタン
        self.button01 = QtWidgets.QPushButton("make Sphere")
        layout.addWidget(self.button01)

        # layout
        self.setLayout(layout)

        # signal
        self.button01.clicked.connect(lambda: self.make_sphere(self.button01.text()))
        # 下記をラムダ式にしたもの。button01の名前をmake_sphere関数に渡しています
        # self.button01.clicked.connect(self.make_sphere)

    def make_sphere(self, button_name):
        logger.debug(button_name)
        count = int(self.spinBox.value())  # spinBoxの値を取得
        logger.debug(count)

        for i in range(count):  # 取得した値を0から順番にfor文で回します
            pm.polySphere()  # sphereを作成

            x_pos = i * 5  # x座標
            y_pos = i * 1  # y座標

            pm.move([x_pos, y_pos, 0])  # 更にsphereの位置を変更


if __name__ == '__main__':
    form = Form()
    form.show()
